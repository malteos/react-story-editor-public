import {IChart, IPosition} from "@mrblenny/react-flow-chart/src";
import {IDocument} from "./documents";
import {ISegment} from "./segments";
import {IRelation} from "./relations";
import {ITopic} from "./topics";


export interface IStoryUnit<UnitType> {
    object: UnitType;
    position: IPosition;
    rank?: number;
    relevance?: number;
}

export interface IStory {
    documents: IStoryUnit<IDocument>[];
    segments: IStoryUnit<ISegment>[];
    topics: IStoryUnit<ITopic>[];
    relations: IRelation[];
}

// Ports
export const defaultPorts = {
    port1: {
        id: 'port1',
        type: 'input',
    },
    port2: {
        id: 'port2',
        type: 'output',
    },
};

export const topicPorts = {
    port2: {
        id: 'port2',
        type: 'output',
    }
};

export function getChartFromStory(
    story: IStory,
): IChart {
    console.log('getChartFromStory ', story);

    const chart: IChart = {
        offset: {
            x: 100,
            y: 100,
        },
        scale: 1.0,
        nodes: {},
        links: {},
        selected: {},
        hovered: {},
    };

    // From relations to links
    story.relations.forEach(unit => {
        const id = unit.fromId + '_' + unit.toId;

        // const fromPort = unit.fromId.indexOf('moabit') ? 'port1' : 'port2';

        chart.links[id] = {
            id,
            from: {
                nodeId: unit.fromId,
                portId: 'port2'
            },
            to: {
                nodeId: unit.toId,
                portId: 'port1',
            },
            properties: {
                label: unit.label
            }
        }
    });

    // From topics to nodes
    story.topics.forEach(unit => {
       chart.nodes[unit.object.id] = {
           id: unit.object.id,
           type: 'topic',
           position: unit.position,
           ports: topicPorts,
           properties: Object.assign({rank: unit.rank}, unit.object)
       }
    });
    // From segments to nodes
    story.segments.forEach(unit => {
        chart.nodes[unit.object.id] = {
            id: unit.object.id,
            type: 'segment',
            position: unit.position,
            ports: defaultPorts,
            properties: Object.assign({rank: unit.rank, relevance: unit.relevance}, unit.object)
        }
    });
    // From documents to nodes
    story.documents.forEach(unit => {
        chart.nodes[unit.object.id] = {
            id: unit.object.id,
            type: 'document',
            position: unit.position,
            ports: defaultPorts,
            properties: Object.assign({rank: unit.rank}, unit.object)
        }
    });


    return chart;
}
