export interface IRelation {
    fromId: string;
    toId: string;
    label: string;
}

