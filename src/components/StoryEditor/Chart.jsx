import React from "react";
import ReactDOM from "react-dom";
import * as d3 from "d3";

const updateNode = selection => {
    selection.attr("transform", d => "translate(" + d.x + "," + d.y + ")");
};

let drag = d3
    .drag()
    .subject(d => {
        return d;
    })
    .on("drag", function(d) {
        var x = d3.event.x;
        var y = d3.event.y;
        console.log(d, x, y);
        d3.select(this).attr(
            "transform",
            "translate(" + (d.x = x) + "," + (d.y = y) + ")"
        );
    });

let enterNode = selection => {
    selection
        .select("rect")
        .attr("width", d => d.width)
        .attr("height", d => d.height)
        .attr("transform", d => "translate(" + d.x + "," + d.y + ")")
        .call(drag);
};

class Rect extends React.Component {
    componentDidMount() {
        this.d3Node = d3
            .select(ReactDOM.findDOMNode(this))
            .datum(this.props.dataitem)
            .call(enterNode);
    }

    render() {
        return (
            <g className="node">
                <rect />
            </g>
        );
    }
}

class Graph extends React.Component {
    //   componentDidMount() {
    //     this.d3Graph = d3.select(ReactDOM.findDOMNode(this));
    //   }

    render() {
        var boxes = [];
        this.props.data.forEach(function(d) {
            boxes.push(<Rect dataitem={d} />);
        });

        return (
            <svg width={300} height={300}>
                <g>{boxes}</g>
            </svg>
        );
    }
}

//##########################################
class Chart extends React.Component {
    render() {
        var premdata = [
            {
                width: 10,
                height: 30,
                x: 30,
                y: 30
            },
            {
                width: 10,
                height: 30,
                x: 50,
                y: 50
            }
        ];

        return (
            <div>
                My Data <br />
                <Graph data={premdata} />
            </div>
        );
    }
}

Chart.defaultProps = {
    chart: "loading"
};

export default Chart;
