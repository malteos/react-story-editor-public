import React from "react";
import {INode, ISelectedOrHovered} from '@mrblenny/react-flow-chart/src'
import {SelectedTopic} from "./SelectedTopic";
import {SelectedDocument} from "./SelectedDocument";
import {SelectedSegment} from "./SelectedSegment";


export type ChartChangeCallback = (changedChart: any) => void;

export type SelectOnChartCallback = (selected: ISelectedOrHovered) => void;

export interface ISelectedNode {
    nodeId: string | undefined,
    stateActions: any,
    handleChartChange: ChartChangeCallback,
    selectOnChart: SelectOnChartCallback,
    node: INode,
}

export const SelectedNode = ({ nodeId, stateActions, handleChartChange, selectOnChart, node }: ISelectedNode) => {
    if(node.type === 'topic') {
        return (<SelectedTopic nodeId={nodeId} stateActions={stateActions} handleChartChange={handleChartChange} selectOnChart={selectOnChart} node={node} />);
    } else if(node.type === 'document') {
        return (<SelectedDocument nodeId={nodeId} stateActions={stateActions} handleChartChange={handleChartChange} selectOnChart={selectOnChart} node={node} />);
    } else if(node.type === 'segment') {
        return (<SelectedSegment nodeId={nodeId} stateActions={stateActions} handleChartChange={handleChartChange} selectOnChart={selectOnChart} node={node} />);
    } else {
        return <p>UNKNOWN NODE TYPE</p>;
    }
};
