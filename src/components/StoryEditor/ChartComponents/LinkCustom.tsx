import {
    generateCurvePath,
    generateRightAnglePath,
    generateSmartPath,
    ILinkDefaultProps
} from "@mrblenny/react-flow-chart/src";
import {RegularLink} from "@mrblenny/react-flow-chart/src/components/Link/variants";
import * as React from 'react';
import {LinkLabel} from "./LinkLabel";
import {LabelContent} from "./LabelContent";

export const LinkCustom = (props: ILinkDefaultProps) => {
    const { config, startPos, endPos, fromPort, toPort, matrix } = props;
    const points = config.smartRouting
        ? !!toPort && !!matrix
            ? generateSmartPath(matrix, startPos, endPos, fromPort, toPort)
            : generateRightAnglePath(startPos, endPos)
        : generateCurvePath(startPos, endPos);

    const linkColor: string =
        (fromPort.properties && fromPort.properties.linkColor) || 'cornflowerblue';

    const linkProps = {
        // config,
        points,
        linkColor,
        // startPos,
        // endPos,
        ...props,
    };

    // const { startPos, endPos, onLinkClick, link } = linkProps;
    const centerX = startPos.x + (endPos.x - startPos.x) / 2;
    const centerY = startPos.y + (endPos.y - startPos.y) / 2;

    return (
        <>
            <RegularLink {...linkProps} />

            <LinkLabel
                style={{ left: centerX, top: centerY }}
                >
                { props.link.properties && props.link.properties.label && (
                    <LabelContent
                        onClick={e => {
                            // e.stopPropagation();
                            // alert('y');
                        }}
                    >{props.link.properties && props.link.properties.label}</LabelContent>
                )}
                {/*<LinkButton*/}
                {/*    onClick={(e: any) => {*/}
                {/*        e.stopPropagation();*/}
                {/*        alert('X');*/}
                {/*    }}*/}
                {/*>*/}
                {/*    x*/}
                {/*</LinkButton>*/}
            </LinkLabel>
        </>
    );
};
