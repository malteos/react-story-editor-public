import styled from 'styled-components'

export const Sidebar = styled.div`
  width: 500px;
  background: white;
  display: flex;
  flex-direction: column;
  flex-shrink: 0;
`;
