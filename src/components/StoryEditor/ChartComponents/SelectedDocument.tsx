import React from "react";
import {Button, Tag} from "@blueprintjs/core";
import {ISelectedNode} from "./SelectedNode";


export const SelectedDocument = ({ nodeId, stateActions, handleChartChange, selectOnChart, node }: ISelectedNode) => {

    const {title, subtitle, description} = node.properties;

    return (
        <div className="selected-node-wrapper">
            <div className="selected-node" data-id={nodeId}>
                <div>
                    <Tag>{node.type.toUpperCase()}</Tag>
                    <h2>{title}</h2>
                    <h3>{subtitle}</h3>
                    <p>{description}</p>
                </div>
                <Button
                    intent="primary"
                    icon="document-open"
                    text="Open"
                    />
                <span> </span>
            </div>
        </div>
    )
};
