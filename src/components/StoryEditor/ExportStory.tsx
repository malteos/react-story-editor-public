import React, { useState } from "react";
import {IChart} from '@mrblenny/react-flow-chart/src'
import {Button, Card, Elevation, Overlay} from "@blueprintjs/core";
import {STORY_EXPORT} from "./misc/data";

export interface IExportStory {
    chart: IChart,
}

export const ExportStory = ({ chart }: IExportStory) => {
    const [openOverlay, setOpenOverlay] = useState(false);


    return (
        <>
            <Button
                icon="export"
                text="Export story"
                onClick={() => setOpenOverlay(true)}
            />
            <Overlay
                isOpen={openOverlay}
                canOutsideClickClose={true}
                canEscapeKeyClose={true}
                autoFocus={true}
                hasBackdrop={true}
                usePortal={true}
                onClose={() => setOpenOverlay(false)}
            >
                <Card
                    className="export-story-card"
                    interactive={true}
                    elevation={Elevation.TWO}
                >
                    <h2>Export story</h2>
                    <p>{STORY_EXPORT}</p>

                    <Button icon="floppy-disk" intent="success" text="Save PDF" />
                    &nbsp;
                    <Button  icon="floppy-disk" intent="success" text="Save DOCX" />


                </Card>
            </Overlay>
        </>
    )
};
